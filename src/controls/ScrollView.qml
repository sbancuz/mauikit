/*
 * SPDX-FileCopyrightText: 2017 Marco Martin <mart@kde.org>
 * SPDX-FileCopyrightText: 2017 The Qt Company Ltd.
 * 
 * SPDX-License-Identifier: LGPL-3.0-only OR GPL-2.0-or-later
 */


import QtQuick 2.9
import QtQuick.Controls 2.15

ScrollView
{
    id: control       
}
